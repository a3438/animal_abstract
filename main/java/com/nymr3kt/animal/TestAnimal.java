/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal;

/**
 *
 * @author nymr3kt
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.run();
        h1.sleep();
        h1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
        System.out.println("---------------------------------------------------------------------------------------------------");
        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is repitile animal ? " + (a1 instanceof Raptile));
        System.out.println("---------------------------------------------------------------------------------------------------");

        Dog d1 = new Dog("Dee");
        d1.eat();
        d1.run();
        d1.sleep();
        d1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("d1 is animal ?" + (d1 instanceof Animal));
        System.out.println("d1 is Land animal ?" + (d1 instanceof LandAnimal));
        System.out.println("---------------------------------------------------------------------------------------------------");

        Cat c1 = new Cat("Caca");
        c1.eat();
        c1.run();
        c1.sleep();
        c1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("c1 is animal ?" + (c1 instanceof Animal));
        System.out.println("c1 is Land animal ?" + (c1 instanceof LandAnimal));
        System.out.println("---------------------------------------------------------------------------------------------------");

        Crocodile cro1 = new Crocodile("Chocho");
        cro1.crawl();
        cro1.eat();
        cro1.sleep();
        cro1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("cro1 is animal ?" + (cro1 instanceof Animal));
        System.out.println("cro1 is raptile ?" + (cro1 instanceof Raptile));
        System.out.println("---------------------------------------------------------------------------------------------------");
        
        Snake s1 = new Snake("Sese");
        s1.crawl();
        s1.eat();
        s1.sleep();
        s1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("s1 is animal ?" + (s1 instanceof Animal));
        System.out.println("s1 is raptile ?" + (s1 instanceof Raptile));
        System.out.println("---------------------------------------------------------------------------------------------------");
        
        Fish f1 = new Fish("FuFu") ;
        f1.eat();
        f1.swim();
        f1.sleep();
        f1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("f1 is animal ?" + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ?" + (f1 instanceof AquaticAnimal));
        System.out.println("---------------------------------------------------------------------------------------------------");
        
        Crab cr1 = new Crab("Cucu");
        cr1.eat();
        cr1.swim();
        cr1.sleep();
        cr1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("cr1 is animal ?" + (cr1 instanceof Animal));
        System.out.println("cr1 is aquatic animal ?" + (cr1 instanceof AquaticAnimal));
        System.out.println("---------------------------------------------------------------------------------------------------");
        
        Bat b1 = new Bat("Bubu");
        b1.eat();
        b1.fly();
        b1.sleep();
        b1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("b1 is animal ?" + (b1 instanceof Animal));
        System.out.println("b1 is poultry ?" + (b1 instanceof Poultry));
        System.out.println("---------------------------------------------------------------------------------------------------");
        
        Bird bi1 = new Bird("Bibi");
        bi1.eat();
        bi1.fly();
        bi1.sleep();
        bi1.speak();
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("bi1 is animal ?" + (bi1 instanceof Animal));
        System.out.println("bi1 is poultry ?" + (bi1 instanceof Poultry));
        System.out.println("---------------------------------------------------------------------------------------------------");
        

    }
}
