/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal;

/**
 *
 * @author nymr3kt
 */
public abstract class LandAnimal extends Animal{

    public LandAnimal(String name, int numberOfLegs) {
        super(name, numberOfLegs);
    }

    public abstract void run() ;
}
