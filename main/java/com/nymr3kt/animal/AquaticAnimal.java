/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal;

/**
 *
 * @author nymr3kt
 */
public abstract class AquaticAnimal extends Animal{

    public AquaticAnimal(String name,int numberOfLeg) {
        super(name,numberOfLeg);
    }
    public abstract void swim();
    
}
