/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal;

/**
 *
 * @author nymr3kt
 */
public abstract class Animal {
    private String name ;
    private int numberOfLegs ;

    public Animal(String name, int numberOfLegs) {
        this.name = name;
        this.numberOfLegs = numberOfLegs;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numberOfLegs=" + numberOfLegs + '}';
    }
     public abstract void eat();
     public abstract void walk();
     public abstract void speak();
     public abstract void sleep();
}
