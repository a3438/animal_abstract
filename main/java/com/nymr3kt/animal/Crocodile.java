/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.animal;

/**
 *
 * @author nymr3kt
 */
public class Crocodile extends Raptile{
    private final String nickname ;

    public Crocodile(String nickname) {
        super("Crocodile",4);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
       System.out.println("Crocodile : " + nickname + " craw");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile : " + nickname + " sleep");
    }
    
}
